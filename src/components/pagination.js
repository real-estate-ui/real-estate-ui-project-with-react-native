import React from 'react';
import PropTypes from 'prop-types';
import configs from '../utils/configs'
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    bottom:0,
    flexDirection: 'row',
    marginVertical: height * 0.0125,
    justifyContent: 'center',
    left: width * 0.25,
    right: width * 0.25,
  },
  pagination: {
    width: width * 0.0267,
    height: width * 0.0267,
    borderRadius: 10,
    marginHorizontal: width * 0.025,
  },
});

const Pagination = ({
  size,
  paginationIndex,
  scrollToIndex,
  paginationDefaultColor,
  paginationActiveColor,
}) => {
  return (
    <View style={styles.paginationContainer}>
      {Array.from({ length: size }).map((_, index) => (
        <TouchableOpacity
          style={[
            styles.pagination,
            paginationIndex === index
              ? { backgroundColor: configs.primaryColor }
              : { backgroundColor: paginationDefaultColor },
          ]}
          key={index}
          onPress={() => scrollToIndex({ index })}
        />
      ))}
    </View>
  );
};

Pagination.propTypes = {
  scrollToIndex: PropTypes.func.isRequired,
  size: PropTypes.number.isRequired,
  paginationIndex: PropTypes.number,
  paginationActiveColor: PropTypes.string,
  paginationDefaultColor: PropTypes.string,
};

Pagination.defaultProps = {
  data: [],
  paginationIndex: 0,
  paginationActiveColor: 'black',
  paginationDefaultColor: 'white',
};

export default Pagination;