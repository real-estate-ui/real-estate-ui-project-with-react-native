import React from 'react';
import { TextInput } from "react-native";
import configs from '../utils/configs';

export default SearchBox = (props) => {
    
    const {style} = props;
    
    return (
        <TextInput
            style={[configs.style.formInput2,style]}
            onChangeText={(text) => props.onChangeText(text)}
            placeholder={props.placeholderText}
            value={props.value}
            onSubmitEditing={() => {
                // this.doSearch();
            }}
            underlineColorAndroid={'transparent'}
        />
    );
}