import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../screens/home';
import Search from '../screens/search';
import Profile from '../screens/profile';
import Menu from '../screens/menu';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

export default class BottomTab extends React.Component {
    render(){
        return(
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;
            
                        if (route.name === 'HomePage') {
                            iconName = focused
                            ? 'home'
                            : 'home-outline';
                        } else if (route.name === 'Search') {
                            iconName = focused ? 'search-outline' : 'search-outline';
                        } else if (route.name === 'Profile') {
                            iconName = focused ? 'md-person' : 'md-person-outline';
                        } else if (route.name === 'Menu') {
                            iconName = focused ? 'menu-outline' : 'menu-outline';
                        }
            
                        return <Ionicons name={iconName} size={size} color={color} />;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}
            >
                <Tab.Screen component={Home} name="HomePage" options={{ title:'Home',}}/>
                <Tab.Screen component={Search} name="Search" />
                <Tab.Screen component={Profile} name="Profile" />
                <Tab.Screen component={Menu} name="Menu" />
            </Tab.Navigator>
        )
    }
}