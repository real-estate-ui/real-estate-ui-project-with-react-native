import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BottomTab from './bottomTabs';

const Stack = createStackNavigator();

export default class MainStack extends Component {
    render() {
        return (
            <Stack.Navigator screenOptions={{headerShown:false}}>
                <Stack.Screen component={BottomTab} name="Home" options={{ headerTransparent:true, }}/>
            </Stack.Navigator>
        )
    }
}
