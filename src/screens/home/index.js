import React, { Component } from 'react';
import { View, Text,ScrollView,SafeAreaView,StatusBar,StyleSheet,ImageBackground } from 'react-native';
import configs from '../../utils/configs';
import SwiperFlatList from 'react-native-swiper-flatlist';
import data from '../../mockData/carouselImage';
import Pagination from '../../components/pagination';
import Carousel from 'react-native-snap-carousel';

export default class Home extends Component {

    state={
        searchText:'',
        activeIndexForHighlight:0,
    }

    onChangeText(text){
        console.log(text);
    }

    _renderItem({item,index}){
        return(
            <View key={index} style={{backgroundColor:'blue',height:configs.height/ 3,width:configs.width,}}>
                <ImageBackground source={{uri:item.url}} style={{height:'100%',width:'100%',borderRadius:15}} />
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView>
                <StatusBar translucent backgroundColor="transparent" />
                <ScrollView horizontal={false} showsVerticalScrollIndicator={false}>
                    <View>
                        <SwiperFlatList
                            autoplay
                            autoplayDelay={3}
                            autoplayLoop
                            showPagination
                            PaginationComponent={Pagination}
                        >
                            {
                                data.carouselList.map((item,index)=>(
                                    <ImageBackground key={index} source={{uri:item.url}} style={styles.carousel} />
                                ))
                            }
                        </SwiperFlatList>
                    </View>
                    <View style={{padding:10}}>
                        <Carousel
                            layout={"stack"}
                            ref={ref => this.carousel = ref}
                            data={data.highlight}
                            itemWidth={configs.width-30}
                            sliderWidth={configs.width}
                            renderItem={this._renderItem}
                            onSnapToItem = { index => this.setState({activeIndexForHighlight:index}) }
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    carousel:{
        width:configs.width,
        height:configs.height / 3 +70,
        resizeMode:'contain'
    },
})
