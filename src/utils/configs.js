import { Dimensions,Platform } from 'react-native';

const $primaryColor = '#EFAB0E';
const $layout = 'light';
const $borderRadius = 10;

const { width , height } = Dimensions.get('window');

const configs = {
    width,
    height,
    primaryColor:$primaryColor,
    layout:$layout,
    defaultBorderRadius:$borderRadius,
    defaultFontSize: Platform.OS == 'ios' ? 16 : 14,
	layoutMode: $layout,
	backgroundColor: $layout =='dark' ? '#222222' : '#f2f2f2',
	listBackgroundColor: $layout =='dark' ? '#111111' : '#ffffff',
	listSeparatorColor: $layout =='dark' ? '#222222' : '#eeeeee',
	tabBarColor: $layout == 'dark' ? '#111111' : '#fffffff',
	cardColor: $layout == 'dark' ? '#131313' : '#ffffff',
	defaultFontColor: $layout == 'dark' ? '#f2f2f2' : '#4f555f',

    style:{
        formInput2: {
			color: '#111111', 
			fontSize: 15, 
			borderBottomWidth: 0, 
			backgroundColor: '#ffffff',
			borderRadius: $borderRadius + 6,
			paddingHorizontal: 12,
			marginTop: 5,
			height: 45
		},
    }
}

export default configs;